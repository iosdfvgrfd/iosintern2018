//
//  ListOpenningViewController.swift
//  Simple Test
//
//  Created by Phung Duc Chinh on 6/11/18.
//  Copyright © 2018 Phung Duc Chinh. All rights reserved.
//

import UIKit

class ListOpenningViewController: UIViewController {
    
    @IBOutlet weak var tbvListDate: UITableView!
    
    var list = ListDateOpenning.list
    var dateTime : DateOpenning?
    let current = CurrentTime.currentTime()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tbvListDate.delegate = self
        tbvListDate.dataSource = self
        self.navigationItem.title = "Lịch khai giảng"
        print(current)
        
        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension ListOpenningViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listOpen.list.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ListOpenningCell") as! ListOpenningCell
        let date = listOpen.list[indexPath.row]
        cell.lblTitle.text = date.name
        cell.lblDate.text = "Khai giảng vào ngày " + (date.date)!
        if current < date.date! {
            print("sap den ngay")
            cell.imgNew?.isHidden = false
            cell.imgNew.image = UIImage(named: "imgNew")
        }else{
            print("da qua ngay")
            cell.imgNew?.isHidden = true
        }
        
        return cell
    }
}
