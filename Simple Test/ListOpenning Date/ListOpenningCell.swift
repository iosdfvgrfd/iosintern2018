//
//  ListOpenningCell.swift
//  Simple Test
//
//  Created by Phung Duc Chinh on 6/11/18.
//  Copyright © 2018 Phung Duc Chinh. All rights reserved.
//

import UIKit

class ListOpenningCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgNew: UIImageView!
    @IBOutlet weak var lblDate: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
