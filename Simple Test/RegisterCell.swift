//
//  RegisterCell.swift
//  Simple Test
//
//  Created by Phung Duc Chinh on 6/6/18.
//  Copyright © 2018 Phung Duc Chinh. All rights reserved.
//

import UIKit

class RegisterCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBOutlet weak var imgCell: UIImageView!
    @IBOutlet weak var txfCell: UITextField!
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
