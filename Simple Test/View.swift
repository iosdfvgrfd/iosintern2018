//
//  View.swift
//  Simple Test
//
//  Created by Phung Duc Chinh on 6/5/18.
//  Copyright © 2018 Phung Duc Chinh. All rights reserved.
//

import UIKit

class View: UIView {

    @IBOutlet weak var lblLogin: UILabel!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    @IBAction func btnLogin(_ sender: Any) {
        lblLogin.text = "Have a nice day!"
    }
    
    @IBAction func btnBack(_ sender: Any) {
        lblLogin.text = "Sign in"
    }
}
