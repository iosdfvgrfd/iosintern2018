//
//  Answer.swift
//  Simple Test
//
//  Created by Phung Duc Chinh on 7/12/18.
//  Copyright © 2018 Phung Duc Chinh. All rights reserved.
//

import Foundation
let jsonKey : [[String: Any]] = [
    [
        "idkey": 10,
        "name" : "those",
        "idques" : 0
    ],
    [
        "idkey": 1,
        "name" : "theme",
        "idques" : 0
    ],
    [
        "idkey": 2,
        "name" : "their",
        "idques" : 0
    ],
    [
        "idkey": 3,
        "name" : "that",
        "idques" : 0
    ],
    [
        "idkey": 4,
        "name" : "suggests",
        "idques" : 1
    ],
    [
        "idkey": 5,
        "name" : "wishes",
        "idques" : 1
    ],
    [
        "idkey": 6,
        "name" : "needs",
        "idques" : 1
    ],
    [
        "idkey": 7,
        "name" : "requires",
        "idques" : 1
    ],
    [
        "idkey": 8,
        "name" : "wishing",
        "idques" : 2
    ],
    [
        "idkey": 9,
        "name" : "welcoming",
        "idques" : 2
    ],
    [
        "idkey": 10,
        "name" : "giving",
        "idques" : 2
    ],
    [
        "idkey": 11,
        "name" : "looking",
        "idques" : 2
    ],
    [
        "idkey": 12,
        "name" : "manufacture",
        "idques" : 3
    ],
    [
        "idkey": 13,
        "name" : "manufacturing",
        "idques" : 3
    ],
    [
        "idkey": 14,
        "name" : "manufacturer",
        "idques" : 3
    ],
    [
        "idkey": 15,
        "name" : "manufactured",
        "idques" : 3
    ],
    [
        "idkey": 16,
        "name" : "promptly",
        "idques" : 4
    ],
    [
        "idkey": 17,
        "name" : "prompts",
        "idques" : 4
    ],
    [
        "idkey": 18,
        "name" : "prompter",
        "idques" : 4
    ],
    [
        "idkey": 19,
        "name" : "prompted",
        "idques" : 4
    ],
    [
        "idkey": 20,
        "name" : "intend",
        "idques" : 5
    ],
    [
        "idkey": 21,
        "name" : "intended",
        "idques" : 5
    ],
    [
        "idkey": 22,
        "name" : "intention",
        "idques" : 5
    ],
    [
        "idkey": 23,
        "name" : "intentional",
        "idques" : 5
    ],
    [
        "idkey": 24,
        "name" : "turn",
        "idques" : 6
    ],
    [
        "idkey": 25,
        "name" : "held",
        "idques" : 6
    ],
    [
        "idkey": 26,
        "name" : "change",
        "idques" : 6
    ],
    [
        "idkey": 27,
        "name" : "alter",
        "idques" : 6
    ],
    [
        "idkey": 28,
        "name" : "initially",
        "idques" : 7
    ],
    [
        "idkey": 29,
        "name" : "initiate",
        "idques" : 7
    ],
    [
        "idkey": 30,
        "name" : "initiation",
        "idques" : 7
    ],
    [
        "idkey": 31,
        "name" : "initial",
        "idques" : 7
    ],
    [
        "idkey": 32,
        "name" : "willing",
        "idques" : 8
    ],
    [
        "idkey": 33,
        "name" : "optimistic",
        "idques" : 8
    ],
    [
        "idkey": 34,
        "name" : "visionary",
        "idques" : 8
    ],
    [
        "idkey": 35,
        "name" : "assertive",
        "idques" : 8
    ],
    [
        "idkey": 36,
        "name" : "speaking",
        "idques" : 9
    ],
    [
        "idkey": 37,
        "name" : "to speak",
        "idques" : 9
    ],
    [
        "idkey": 38,
        "name" : "spoke",
        "idques" : 9
    ],
    [
        "idkey": 39,
        "name" : "has spoken",
        "idques" : 9
    ]
]
