//
//  QuestionViewController.swift
//  Simple Test
//
//  Created by Phung Duc Chinh on 6/6/18.
//  Copyright © 2018 Phung Duc Chinh. All rights reserved.
//

import UIKit
import Alamofire

class QuestionViewController: UIViewController {

    let lblAnswer = ["A.", "B.", "C.","D."]
    var ques = [QuestObject]()
    var answer = [AnswerObject]()
    var key = [""]
    
    var count : Int = 0
    
    var index = IndexPath(row: 1, section: 1)
    
    var dem : Int = 0
    var da : Int = 0
    
    var questtionRetun = ListQuest()
    var answerRetun = ListAnswer()
    var check = [String](repeating: "", count: 10)
    
    @IBOutlet weak var txfQuestion: UITextField!
    @IBOutlet weak var tbvAnswer: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        getAllQuestion()
        getAllAnswer()
        let arrswap = questtionRetun.list.shuffled()
        for i in arrswap{
            print(i.name as! String)
        }
        tbvAnswer.delegate = self
        tbvAnswer.dataSource = self
        tbvAnswer.isScrollEnabled = false
        tbvAnswer.separatorStyle = .none
        let a = getQuset(int : dem)
        txfQuestion.text = a.name
        answer = getAnswer(int : dem)
        print(dem)
        txfQuestion.isEnabled = false
        txfQuestion.sizeToFit()
        
        btnBack.layer.cornerRadius = 7
        btnBack.addTarget(self, action: #selector(BackTarget), for: .touchUpInside)
        btnNext.layer.cornerRadius = 7
        btnNext.addTarget(self, action: #selector(NaxtTarget), for: .touchUpInside)
        count = questtionRetun.list.count
        check = [String](repeating: "", count: count)
        ques = questtionRetun.list
        btnSubmit.addTarget(self, action: #selector(Submit), for: .touchUpInside)
        btnSubmit.layer.cornerRadius = 7
        
        key = getAllKey()
        btnBack.isEnabled = false
        btnNext.isEnabled = false
        btnSubmit.isEnabled = false
        // Do any additional setup after loading the view.
    }
    
    @objc func Submit(){
        da = 0
        
        for i in 0...9{
            if check[i] == key[i] , check[i] != ""{ // swift 3
                da += 1
            }
        }
        print("Your mark is " + "\(da)")
        let mark = String(da)
        let alert = UIAlertController(title: "Điểm của bạn là: ", message: mark, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .default) { action in
            print("da nhan thong tin")
        })
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func BackTarget() {
        print("back")
        index.section = 1
        dem = (dem - 1 + count) % count
        let a = getQuset(int : dem)
        txfQuestion.text = a.name
        answer = getAnswer(int : dem)
        tbvAnswer.reloadData()
    }
    
    @objc func NaxtTarget() {
        print("next")
        index.section = 1
        dem = (dem + 1) % count
        let a = getQuset(int : dem)
        txfQuestion.text = a.name
        answer = getAnswer(int : dem)
        tbvAnswer.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet var btnBack: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnSubmit: UIButton!
    
    func getAllQuestion(){
        if let path = Bundle.main.path(forResource: "data_credit", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult : Array = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves) as! [Dictionary<String, Any>]
               
                for i in jsonResult{
                    
                    let questData = QuestObject(idquest: i["idquest"] as! Int, name: i["name"] as! String, idkey: i["idkey"] as! Int)
                    self.questtionRetun.list.append(questData)
                   
                }
                print(self.questtionRetun.list.count)
            } catch {
                // handle error
            }
        }
    }
    
    func getAllAnswer(){
        if let path = Bundle.main.path(forResource: "data_answer", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult : Array = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves) as! [Dictionary<String, Any>]
               
                for i in jsonResult{
                    let answerData = AnswerObject(idquest: i["idques"] as! Int, name: i["name"] as! String, idkey: i["idkey"] as! Int)
                    answerRetun.list.append(answerData)
                }
                print(answerRetun.list.count)
            } catch {
                // handle error
            }
        }
    }
    
    func getAnswer(int : Int) -> [AnswerObject]{
        var arr = [AnswerObject]()
        for i in answerRetun.list{
            if i.idquest == int{
                arr.append(i)
            }
        }
        print(arr.count)
        return arr
    }
    
    func getQuset(int : Int) -> QuestObject{
        var arr = QuestObject(idquest: int, name: "", idkey: int)
        for i in questtionRetun.list{
            if i.idquest == int{
                arr = i
            }
        }
        return arr
    }
    
    func getAllKey() -> [String]{
        var arrKey = [String]()
        for i in 0...count-1{
            let arr = getAnswer(int: i)
            arrKey.append(arr[ques[i].idkey!-1].name!)
            print(arrKey[i])
        }
        return arrKey
    }
    
}

extension QuestionViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "QuestionCell") as! QuestionCell
        let ans = answer[indexPath.row]
        if indexPath == index || answer[indexPath.row].name == check[dem]{
            cell.txfAnswer.layer.borderWidth = 2
            cell.txfAnswer.layer.borderColor = UIColor.red.cgColor
            cell.txfAnswer.layer.cornerRadius = 7
            cell.txfAnswer.text = lblAnswer[indexPath.row] + " " + ans.name!
            cell.txfAnswer.textColor = UIColor.white
            cell.txfAnswer.backgroundColor = UIColor.red
            cell.txfAnswer.setLeftPaddingPoints(20)
            cell.selectionStyle = .none
        
        }else{
            cell.txfAnswer.text = lblAnswer[indexPath.row] + " " + ans.name!
            cell.txfAnswer.layer.borderWidth = 2
            cell.txfAnswer.layer.borderColor = UIColor.green.cgColor
            cell.txfAnswer.layer.cornerRadius = 7
            cell.txfAnswer.textColor = UIColor.black
            cell.txfAnswer.backgroundColor = UIColor.white
            cell.txfAnswer.setLeftPaddingPoints(20)
            cell.selectionStyle = .none
        }
        
        return cell

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
          tableView.deselectRow(at: indexPath, animated: false)
        
        index = indexPath
        check.remove(at: dem)
        check.insert(answer[indexPath.row].name!,at: dem)
        print("chose" + check[dem])
        btnBack.isEnabled = true
        btnNext.isEnabled = true
        btnSubmit.isEnabled = true
       tbvAnswer.reloadData()
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 52
    }
}
