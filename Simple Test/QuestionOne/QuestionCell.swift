//
//  QuestionCell.swift
//  Simple Test
//
//  Created by Phung Duc Chinh on 6/6/18.
//  Copyright © 2018 Phung Duc Chinh. All rights reserved.
//

import UIKit

class QuestionCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        txfAnswer.isEnabled = false
        // Initialization code
    }

    @IBOutlet weak var txfAnswer: UITextField!
  
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
   

}
