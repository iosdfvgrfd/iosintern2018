//
//  AnswerObject.swift
//  Simple Test
//
//  Created by Phung Duc Chinh on 7/12/18.
//  Copyright © 2018 Phung Duc Chinh. All rights reserved.
//

import Foundation
import UIKit

public class AnswerObject {
    public var idquest : Int?
    public var name : String?
    public var idkey : Int?
    
    init(idquest : Int, name : String, idkey : Int) {
        self.idquest = idquest
        self.name = name
        self.idkey = idkey
    }
}

public class ListAnswer {
    public var list = [AnswerObject]()
    init() {
    }
}
