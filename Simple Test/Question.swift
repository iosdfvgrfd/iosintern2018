//
//  Question.swift
//  Simple Test
//
//  Created by Phung Duc Chinh on 7/12/18.
//  Copyright © 2018 Phung Duc Chinh. All rights reserved.
//

import Foundation

let jsonQues : [[String: Any]] = [

    [
        "idquest": 0,
        "name" : "In order for ------- to provide the necessary benefits, dentists associated with the university's insurance policies must be shown a valid student idquest card.",
        "idkey" : 2
    ],
    [
        "idquest": 1,
        "name" : "CRI Inc. ------- all employees to wear identification badges at all times when inside the plant.",
        "idkey" : 4
    ],
    [
        "idquest": 2,
        "name" : "We are grateful to Mr. Stern for ------- our associates.",
        "idkey" : 2
    ],
    [
        "idquest": 3,
        "name" : "Customers with concerns about the safety of the product are advised to call the ------- as soon as possible.",
        "idkey" : 3
    ],
    [
        "idquest": 4,
        "name" : "Library and information science majors should be reminded of the seminar beginning ------- at 6:00 p.m. in room 212 B.",
        "idkey" : 1
    ],
    [
        "idquest": 5,
        "name" : "Tonight's performance is ------- to raise awareness about our city's urban renewal initiative.",
        "idkey" : 2
    ],
    [
        "idquest": 6,
        "name" : "With the election over, the new candidate could now ------- her attention toward solving the unemployment problem.",
        "idkey" : 1
    ],
    [
        "idquest": 7,
        "name" : "The ------- outlay of assets for the construction of the new streetcar lanes has been cited by newspapers as the main cause of the project's dismissal.",
        "idkey" : 4
    ],
    [
        "idquest": 8,
        "name" : "Debbie Gillespie, chief financial officer of Alabaster Chemicals Ltd., is still about a favorable budget for the company this fiscal year.",
        "idkey" : 2
    ],
    [
        "idquest": 9,
        "name" : "If you wish ------- to me, please make an appointment through my secretary on the 1 0th floor.",
        "idkey" : 2
    ]
    
]

