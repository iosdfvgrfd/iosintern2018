//
//  PostOpenningViewController.swift
//  Simple Test
//
//  Created by Phung Duc Chinh on 6/11/18.
//  Copyright © 2018 Phung Duc Chinh. All rights reserved.
//

import UIKit

class PostOpenningViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var txfTitle: UITextField!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var pkDate: UIDatePicker!
    @IBOutlet weak var btnPost: UIButton!
    
    var strDate = ""
    var tittle = ""
    var date : DateOpenning?
    var id : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "Thêm lịch khai giảng"
        txfTitle.layer.cornerRadius = 7
        pkDate.datePickerMode = .dateAndTime
        pkDate.addTarget(self, action: #selector(handler(sender:)), for: UIControlEvents.valueChanged)
        btnPost.layer.cornerRadius = 7
        btnPost.addTarget(self, action: #selector(PostOpenningDate), for: .touchUpInside)
        txfTitle.addTarget(self, action: #selector(TextFieldChange), for: .editingChanged)
        txfTitle.delegate = self
       
        id = listOpen.list.count
        // Do any additional setup after loading the view.
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @objc func handler(sender: UIDatePicker){
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "yyyy/MM/dd HH:mm"
        strDate = timeFormatter.string(from: pkDate.date)
        self.lblDate.text = strDate
    }
    
    @objc func PostOpenningDate(){
        let a = DateOpenObject(id: id, name: txfTitle.text!, date: lblDate.text!)
        id += 1
        listOpen.list.append(a)
        
        print(listOpen.list.count)
        let alert = UIAlertController(title: "Openning Date ", message: strDate, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Accept", style: .default) { action in
            print("da nhan thong tin")
        })
        self.present(alert, animated: true, completion: nil)
    }

    @objc func TextFieldChange(){
        self.tittle = txfTitle.text!
        print(self.tittle)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool // called when 'return' key pressed. return NO to ignore.
    {
        textField.resignFirstResponder()
        return true
    }
}
