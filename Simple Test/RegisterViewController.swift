//
//  RegisterViewController.swift
//  Simple Test
//
//  Created by Phung Duc Chinh on 6/6/18.
//  Copyright © 2018 Phung Duc Chinh. All rights reserved.
//

import UIKit
import Alamofire

class RegisterViewController: UIViewController {

    @IBOutlet weak var tbvRegister: UITableView!
    
    var  name = ""
    var pass = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tbvRegister.delegate = self
        tbvRegister.dataSource = self
        tbvRegister.allowsSelection = false
        tbvRegister.layer.cornerRadius = 7
        //tbvRegister.register(RegisterCell.self, forCellReuseIdentifier: "Cell")
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    

    @IBAction func btnSubmit(_ sender: Any) {
        tbvRegister.reloadData()
        let index1 = IndexPath(row: 0, section: 0)
        if let cell = tbvRegister.cellForRow(at: index1) as? RegisterCell {
            name = cell.txfCell.text!
        }
        
        let index2 = IndexPath(row: 1, section: 0)
        if let cell = tbvRegister.cellForRow(at: index2) as? RegisterCell {
            tbvRegister.reloadData()
            pass = cell.txfCell.text!
        }
        let alert = UIAlertController(title: name, message:  pass, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .default) { action in
            print("da nhan thong tin")
        })
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    

}


extension RegisterViewController : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as!
            RegisterCell
            
            let img = UIImage(named: "user_icon")!
            cell.imgCell.image = img
            cell.txfCell.placeholder = "Email/ Phone number"
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as!
            RegisterCell
            let img = UIImage(named: "pass_icon")!
            cell.imgCell.image = img
            cell.txfCell.isSecureTextEntry = true
            cell.txfCell.placeholder = "Your password"
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    
}
