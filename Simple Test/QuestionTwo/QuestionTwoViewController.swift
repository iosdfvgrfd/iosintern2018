//
//  QuestionTwoViewController.swift
//  Simple Test
//
//  Created by Phung Duc Chinh on 6/7/18.
//  Copyright © 2018 Phung Duc Chinh. All rights reserved.
//

import UIKit

class QuestionTwoViewController: UIViewController {
    let ques = ["Question 1: 1 + 2 = ?", "Question 2: 4 + 5 = ?"]
    let answer = ["3","5","7","9"]
    let key = ["3"]
    
    var index = IndexPath(row: 1, section: 1)
    
    @IBOutlet weak var txfQuestion: UITextView!
    @IBOutlet weak var tbvQuestion: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tbvQuestion.delegate = self
        tbvQuestion.dataSource = self
        txfQuestion.text = ques[1]
        
       
        
        // Chúng ta phải truyền vào Event và một block, block này sẽ được callback khi dữ liệu thay đổi
        
        ref.observeEventType(.Value, withBlock: { snapshot in
            
            print(snapshot.value)
            
        }, withCancelBlock: { error in
            
            print(error.description)
            
        })
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    @IBAction func btnBack(_ sender: Any) {
        print("back")
    }
    
    @IBAction func btnNext(_ sender: Any) {
        print("Next")
    }
    
    let i = 0
    
   
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension QuestionTwoViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "QuestionCellTwo") as! QuestionCellTwo
        cell.btnButton.setTitle(answer[indexPath.row], for: .normal)
        if indexPath == indexPath {
            cell.btnButton.backgroundColor = UIColor.red
            cell.btnButton.setTitleColor(UIColor.white, for: .normal)
            print(indexPath.row)
        }
        return cell
    }
    
 
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}
