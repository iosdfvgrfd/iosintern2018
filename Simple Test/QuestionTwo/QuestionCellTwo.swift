//
//  QuestionCellTwo.swift
//  Simple Test
//
//  Created by Phung Duc Chinh on 6/7/18.
//  Copyright © 2018 Phung Duc Chinh. All rights reserved.
//

import UIKit

class QuestionCellTwo: UITableViewCell {

    @IBOutlet weak var btnButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        btnButton.layer.cornerRadius = 7
        btnButton.layer.borderWidth = 2
        btnButton.layer.borderColor = UIColor.green.cgColor
        btnButton.backgroundColor = UIColor.white
        btnButton.setTitleColor(UIColor.black, for: .normal)
        // Configure the view for the selected state
    }

}
